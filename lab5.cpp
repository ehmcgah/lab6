/*
Ethan McGaha
ehmcgah
Lab 5
Lab Section: 2
TA: Hanjie Liu
*/
#include <iostream>
#include <iomanip>
#include <algorithm>
#include <sstream>
#include <string>
#include "stdlib.h"
#include "string.h"
using namespace std;
enum Suit { SPADES=0, HEARTS=1, DIAMONDS=2, CLUBS=3 };

typedef struct Card {
  Suit suit;
  int value;
} Card;

string get_suit_code(Card& c);
string get_card_name(Card& c);
bool suit_order(const Card& lhs, const Card& rhs);
int myrandom (int i) { return std::rand()%i;}


int main(int argc, char const *argv[]) {
  /*This is to seed the random generator */
  srand(unsigned (time(0)));

  /*Create a deck of cards of size 52 (hint this should be an array) and
   *initialize the deck*/
   //Initial Creation of 52 deck card array
   Card cardArr[52];
   //for loop creates a sorted deck of 52 cards with value and suit initialized
   for(int i =0; i < 13; ++i) {
     cardArr[i].suit = SPADES;
     cardArr[i].value = (i%13 +2);
   }
   for(int i =13; i < 26; ++i) {
     cardArr[i].suit = HEARTS;
     cardArr[i].value = (i%13 +2);
   }
   for(int i =26; i < 39; ++i) {
     cardArr[i].suit = DIAMONDS;
     cardArr[i].value = (i%13 +2);
   }
   for(int i =39; i < 52; ++i) {
     cardArr[i].suit = CLUBS;
     cardArr[i].value = (i%13 +2);
   }

  /*After the deck is created and initialzed we call random_shuffle() see the
   *notes to determine the parameters to pass in.*/
   //random shuffle passed in pointer to start and end of card array
  std::random_shuffle (&cardArr[0], &cardArr[52], myrandom);
   /*Build a hand of 5 cards from the first five cards of the deck created
    *above*/
    //created an array of five cards drawn from 52 card deck
  Card newHand[5] = {cardArr[0], cardArr[1], cardArr[2],
     cardArr[3], cardArr[4]};

    /*Sort the cards.  Links to how to call this function is in the specs
     *provided*/
    //cards sorted using c++ stdsort function, passed in two pointers and function
  std::sort (&newHand[0], &newHand[5], suit_order);

    /*Now print the hand below. You will use the functions get_card_name and
     *get_suit_code */
     //temp string created to hold value returned by function
     string temp = "nothing";
     //for loop loops through 5 card deck to print
     for(int i = 0; i < 5; ++i) {
       //if statement checks whether value is face card or number
       if(newHand[i].value <= 10) {
         //when number print the value, set width and right alignment
         cout << std::right << setw(10) << newHand[i].value << " of ";
       }
       else {
         //when face card call function to recieve string
         temp = get_card_name(newHand[i]);
         //print temp string
         cout << std::right << setw(10) << temp << " of ";
       }
       //get suit symbol from suit code function
       string suitTemp = get_suit_code(newHand[i]);
       //print out symbol
       cout << suitTemp << endl;
     }

  return 0;
}


/*This function will be passed to the sort funtion. Hints on how to implement
* this is in the specifications document.*/
bool suit_order(const Card& lhs, const Card& rhs) {
  if(lhs.suit < rhs.suit) {
    return true;
  }
  else if(lhs.suit == rhs.suit) {
    if(lhs.value < rhs.value){
      return true;
    }
    else {
      return false;
    }
  }
  else {
    return false;
  }
}
/* This function takes a card from the 5 deck array and returns the symbol
  for the suit using a switch statement.
*/
string get_suit_code(Card& c) {
  switch (c.suit) {
    case SPADES:    return "\u2660";
    case HEARTS:    return "\u2661";
    case DIAMONDS:  return "\u2662";
    case CLUBS:     return "\u2663";
    default:        return "";
  }
}
/* This function takes the passed in card from the deck of 5, and returns
   the string name for a face card by checking the card's value.
*/
string get_card_name(Card& c) {
string temp = "nothing";
switch(c.value) {
  case 11:
    temp = "Jack";
    break;
  case 12:
    temp = "King";
    break;
  case 13:
    temp = "Queen";
    break;
  case 14:
    temp = "Ace";
    break;
  default:
    break;
  }
  return temp;
}
